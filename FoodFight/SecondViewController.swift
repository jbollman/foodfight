//
//  SecondViewController.swift
//  FoodFight
//
//  Created by Joseph Bollman on 6/6/19.
//  Copyright © 2019 Black Mesa. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    
    @IBAction func Tacos(_ sender: UIButton) {
        print("Tacos button")
    }
    
    
    @IBAction func Thai(_ sender: UIButton) {
        print("Thai button")
    }
    
    
    @IBAction func Pizza(_ sender: UIButton) {
        print("Pizza button")
    }
    
    
    @IBAction func Sandwiches(_ sender: UIButton) {
         print("Sandwich button")
    }

    @IBAction func Seafood(_ sender: UIButton) {
        print("Seafood button")
    }
    
    
    @IBAction func Chinese(_ sender: UIButton) {
        print("Chinese button")
    }
    
    @IBAction func Sushi(_ sender: UIButton) {
        print("Sushi button")
    }
    
    @IBAction func Ramen(_ sender: UIButton) {
        print("Ramen button")
    }
    
    @IBAction func Brunch(_ sender: UIButton) {
        print("Brunch button")
    }
    
    @IBAction func BBQ(_ sender: UIButton) {
        print("BBQ button")
    }
    
    @IBAction func Italian(_ sender: UIButton) {
        print("Italian button")
    }
    
    @IBAction func Wings(_ sender: UIButton) {
        print("Wings button")
    }
    
    @IBAction func Searchfood(_ sender: UITextField) {
        print("Searchfood button")
    }
    
 
    @IBAction func FoodLocation(_ sender: UITextField) {
        print("FoodLocation button")
    }
    
}

